import argparse
import posixpath
import csv

# TODO
# Recomendar el unit_step para los labels de las graficas

parser = argparse.ArgumentParser(
    description="Get important data values for animations"
)

parser.add_argument("data", type=str, action="store")
parser.add_argument("-r", action="store", dest="activate_range", type=str)
args = parser.parse_args()
data = posixpath.join(*args.data.split("\\"))

def get_coords_from_csv_fixed(file_name,x_min=None,x_max=None):
    coords = []
    x_min_coord = 0.0
    x_max_coord = 0.0
    count = 0
    with open(f'{file_name}.csv', 'r') as csvFile:
        reader = csv.reader(csvFile)
        for row in reader:
            x,y = row
            coord = [float(x),float(y)]
            if count == 0:
                y_min_coord = float(y)
                y_max_coord = float(y)
            if x_min != None and x_max != None:
                if x_min <= coord[0] <= x_max:
                    if coord[1] < y_min_coord:
                        y_min_coord = coord[1]
                        x_min_coord = coord[0]
                    if y_max_coord < coord[1]:
                        y_max_coord = coord[1]
                        x_max_coord = coord[0]
                    coords.append(coord)
            else:
                if coord[1] < y_min_coord:
                    y_min_coord = coord[1]
                    x_min_coord = coord[0]
                if y_max_coord < coord[1]:
                    y_max_coord = coord[1]
                    x_max_coord = coord[0]
                coords.append(coord)
            count += 1
    csvFile.close()
    max_values = [ [y_max_coord, x_max_coord] , [y_min_coord, x_min_coord] ]
    return coords, max_values

def get_dt(file_name):
    x_coord = []
    with open(f'{file_name}.csv', 'r') as csvFile:
        reader = csv.reader(csvFile)
        count = 0
        for row in reader:
            x,y = row
            x_coord.append(float(x))
            count += 1
            if count >=2:
                break
    csvFile.close()
    return x_coord[1] - x_coord[0]

def main():
    print("Data:\t\t",data)
    # GET DATA
    if args.activate_range:
        x_min,x_max = args.activate_range.split(",")
        x_min = float(x_min)
        x_max = float(x_max)
        print("Range:")
        print("\tx_min:\t",x_min)
        print("\tx_max:\t",x_max)
        coords,max_values = get_coords_from_csv_fixed(data,x_min,x_max)
    else:
        coords,max_values = get_coords_from_csv_fixed(data)
    max_coords, min_coords = max_values
    y_max, x_max = max_coords
    y_min, x_min = min_coords
    dt_data = get_dt(data)
    # PRINT DATA
    print("*** dt DATA ***")
    print("dt_data:\t", dt_data)
    print("*** MAX VALUES ***")
    print("y_max:\t", y_max)
    print("at")
    print("x_max:\t", x_max)
    print("*** MIN VALUES ***")
    print("y_min:\t", y_min)
    print("at")
    print("x_min:\t", x_min)

if __name__ == "__main__":
    main()