import os
import sys
import platform
import glob
import csv
import argparse
import posixpath
import numpy as np
#-------------------- PARSER
parser = argparse.ArgumentParser(
    description="Get important data values for animations"
)
parser.add_argument("file", type=str, action="store")
parser.add_argument("-s", action="store", dest="rango", type=str)
parser.add_argument("-f", action="store", dest="flags", type=str)
parser.add_argument("-d", action="store", dest="d_d", type=str)
parser.add_argument("-i", action="store", dest="initial_shift", type=str)
parser.add_argument("-t", action="store_true", dest="time")
parser.add_argument("-r", action="store", dest="resolution", type=str)
parser.add_argument("-n", action="store_false", dest="anim")
args = parser.parse_args()
file = posixpath.join(*args.file.split("\\"))
#*************
if args.flags == None:
    flags = "k"
else:
    flags = args.flags
#*************
if args.rango != None:
    arg_start,arg_end,arg_dt = args.rango.split(",")
else:
    arg_start = 0
    arg_end = 40
    arg_dt = 10
#***************
if args.d_d != None:
    arg_dd = args.d_d
else:
    arg_dd = 8
#***************
if args.time:
    time_str = "time "
else:
    time_str = ""
#***************
if args.resolution == None:
    args_height = 482
    args_fps = 12
else:
    args_height,args_fps = args.resolution.split(",")
#***************
if args.initial_shift == None:
	args_is = -3
else:
	args_is = args.initial_shift
#----------------------------------------

HEIGHT_SCREEN = args_height
FPS = args_fps

DT    = int(arg_dt) # <- STEPS of the animation
START = int(arg_start) # <- DON'T MODIFY
END   = int(arg_end) # <- MODIFY THIS
RANGE = np.arange(
    START,
    END + DT,
    DT
)
d_data = arg_dd # <- x_max - x_min

# INITIAL ANIMATION
initial_shift = args_is
start_point = 0
end_point = 0

current_os = platform.system()
BIN = 'python' if current_os == 'Windows' else 'python3'
FILE = file
FLAGS = f"-{flags}"
SCENE = 'TEST1'
TEST_TEMP_FILE = 'TEST_TEMP.py'

def get_initial_shift(point):
    return f'INITIAL_SHIFT = {point}'

def get_start_point(point):
    return f'START_POINT = {point}'

def get_end_point(point):
    return f'END_POINT   = {point}'

INITIAL_SHIFT = 'INITIAL_SHIFT = None'
START_POINT   = 'START_POINT   = None'
END_POINT     = 'END_POINT     = None'

FRAME_HEIGHT = 'HEIGHT_SCREEN = None'
FPS_STR      = 'FPS = None'
TARGET       = 'TARGET = None'
START_ANIMATION = 'START = None'

PY_FILE = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    FILE
)

PY_TEST_TEMP_FILE = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    TEST_TEMP_FILE
)
#-----------------------------------------------------
print("**** DATA CONFIG *****")
print("RANGE: ",RANGE)
print("START: ",START)
print("END: ",END)
print("D_X: ",DT)
print("D_DATA: ",d_data)
print("INITIAL_SHIFT: ",initial_shift)
print("**** SCENE CONFIG *****")
print("FRAME_HEIGHT: ",HEIGHT_SCREEN)
print("FPS: ",FPS)
print("TOTAL SCENES: ",(len(RANGE)))
#-----------------------------------------------------
step = 0
for i in RANGE:
    if step > 0:
        initial_shift  = 0
        start_point    = end_point
        end_point      = i
        target = end_point - start_point
        start = False
        run_time = DT

        NEW_INITIAL_SHIFT = get_initial_shift(initial_shift)
        NEW_START_POINT   = get_start_point(start_point)
        NEW_END_POINT     = get_end_point(end_point)
    else:
        target = end_point
        start = True
        NEW_INITIAL_SHIFT = get_initial_shift(initial_shift)
        NEW_START_POINT   = get_start_point(start_point)
        NEW_END_POINT     = get_end_point(end_point)
        run_time = None

    print("-"*50)
    print(f'Scene {step}')
    print(f'{NEW_INITIAL_SHIFT}')
    print(f'{NEW_START_POINT}')
    print(f'{NEW_END_POINT}')
    print("")


    with open(PY_FILE, "r") as infile:
        READ_PY_FILE = infile.read()
        RUN_PY_FILE = READ_PY_FILE.replace(
            INITIAL_SHIFT,
            NEW_INITIAL_SHIFT
        )
        RUN_PY_FILE = RUN_PY_FILE.replace(
            START_POINT,
            NEW_START_POINT
        )
        RUN_PY_FILE = RUN_PY_FILE.replace(
            END_POINT,
            NEW_END_POINT
        )
        #-------------------------
        RUN_PY_FILE = RUN_PY_FILE.replace(
            FRAME_HEIGHT,
            f'HEIGHT_SCREEN = {HEIGHT_SCREEN}'
        )
        RUN_PY_FILE = RUN_PY_FILE.replace(
            FPS_STR,
            f'FPS = {FPS}'
        )
        RUN_PY_FILE = RUN_PY_FILE.replace(
            TARGET,
            f'TARGET = {target}'
        )
        RUN_PY_FILE = RUN_PY_FILE.replace(
            'D_DATA = None',
            f'D_DATA = {d_data}'
        )
        RUN_PY_FILE = RUN_PY_FILE.replace(
            'START = None',
            f'START = {start}'
        )
        RUN_PY_FILE = RUN_PY_FILE.replace(
            'RUN_TIME = None',
            f'RUN_TIME = {run_time}'
        )


    with open(PY_TEST_TEMP_FILE, "w", encoding="utf-8") as outfile:
        outfile.write(RUN_PY_FILE)

    scene_number = format(step, '08d')
    MANIM_RUN = f'{time_str}{BIN} -m manim {PY_TEST_TEMP_FILE} {SCENE} {FLAGS} -o scene_{scene_number}'
    print(MANIM_RUN)
    if args.anim:
        os.system(MANIM_RUN)
    print("-"*50)
    step += 1

FILE_PATH = os.path.dirname(os.path.realpath(__file__))[:-26]
MEDIA_DIR = f'media/videos/{TEST_TEMP_FILE[:-3]}'
VIDEO_DIR = f"{FILE_PATH}{MEDIA_DIR}/{HEIGHT_SCREEN}p{FPS}"

#print(VIDEO_DIR)
#os.system(f"mpv {VIDEO_DIR}/scene_1.mp4")

def concatenate():
    stringa = "ffmpeg -i \"concat:"
    DIR_FILES = f"{VIDEO_DIR}/*.mp4"
    elenco_file_temp = []
    #print(DIR_FILES)
    elenco_video = glob.glob(DIR_FILES)
    os.system(f"ls {DIR_FILES} > active_projects/data_scene/video_list.csv")
    with open(f'active_projects/data_scene/video_list.csv', 'r') as csvFile:
        reader = csv.reader(csvFile)
        for row in reader:
            elenco_file_temp.append(*row)
    csvFile.close()
    #print(elenco_file_temp)
    for f in elenco_video:
        file = f"{VIDEO_DIR}/temp" + str(elenco_video.index(f) + 1) + ".ts"
        os.system("ffmpeg -i " + f + " -c copy -bsf:v h264_mp4toannexb -f mpegts " + file)
        elenco_file_temp.append(file)
    #print(elenco_file_temp)
    for f in elenco_file_temp:
        stringa += f
        if elenco_file_temp.index(f) != len(elenco_file_temp)-1:
            stringa += "|"
        else:
            stringa += f"\" -c copy  -bsf:a aac_adtstoasc {VIDEO_DIR}/output.mp4"
    #print(stringa)
    os.system(stringa)
 
#concatenate()

#os.system(MANIM_RUN)
#os.system(f'python3 {PY_TEST_TEMP_FILE}')
#print("FIN DEL PROCESO")
#print(PY_TEST_TEMP_FILE)