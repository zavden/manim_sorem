from active_projects.data_scene.modules.imports import *
from active_projects.data_scene.scenes.shift_graph import ShiftDataGraph
from active_projects.data_scene.scenes.zoom_axes import ZoomDataAxes

def WAIT(s=1):
    return {"wait": s}

def SHIFT(**kwargs):
    return {
        "shift": kwargs
    }

def ROLL(**kwargs):
    return {
        "roll": kwargs
    }

def ZOOM_CAMERA(**kwargs):
    return {
        "zoom_camera": kwargs
    }

def RESTORE_CAMERA(**kwargs):
    if kwargs == None:
        kwargs = {}
    return {
        "restore_camera": kwargs
    }

def MOVE_CAMERA(**kwargs):
    return {
        "move_camera": kwargs
    }