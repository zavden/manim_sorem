from manimlib.constants import *
from manimlib.mobject.types.vectorized_mobject import VMobject
from manimlib.mobject.types.vectorized_mobject import VGroup
from manimlib.mobject.types.vectorized_mobject import CurvesAsSubmobjects
from manimlib.mobject.geometry import Dot
from manimlib.mobject.functions import ParametricFunction
from manimlib.utils.bezier import interpolate
from manimlib.utils.config_ops import digest_config
from active_projects.data_scene.modules.data_axis import DataAxis
import csv

class DiscreteGraphFromSetPoints(VMobject):
    def __init__(self,set_of_points,**kwargs):
        super().__init__(**kwargs)
        self.set_points_as_corners(set_of_points)

class SmoothGraphFromSetPoints(VMobject):
    def __init__(self,set_of_points,**kwargs):
        super().__init__(**kwargs)
        self.set_points_smoothly(set_of_points)

class DataAxes(VGroup):
    CONFIG = {
        "graph_origin": 5 * LEFT + 0 * DOWN,
        "x_axis_config": {
            "max_size": None,
            "zoom": 1,
            "v_max": 8,
            "v_min": 0,
            "number_frequency": 1,
            "axes_size": 10,
            "min_number": 2,
            "line_config": {},
            "tick_marks_config": {},
            "decimal_number_config": {
                "num_decimal_places": 2,
                "font_config": {
                    "color": RED
                }
            },
            "fade_alpha_size": 0,
            "fade_alpha_shift": 0,
            "avoids_normalization": False,
        },
        "y_axis_config": {
            "max_size": None,
            "zoom": 1,
            "v_max": 0.2,
            "v_min": -0.2,
            "number_frequency": 0.1,
            "axes_size": 6,
            "line_config": {},
            "tick_marks_config": {},
            "decimal_number_config": {
                "num_decimal_places": 2,
                "font_config": {
                    "color": TEAL
                }
            },
            "fade_alpha_size": 0,
            "fade_alpha_shift": 0,
            "avoids_normalization": False,
        },
        "show_lines": True,

    }
    def __init__(self,**kwargs):
        digest_config(self, kwargs)
        VGroup.__init__(self)
        self.x_axis = self.get_fixed_axis("X",**self.x_axis_config)
        self.y_axis = self.get_fixed_axis("Y",**self.y_axis_config)
        self.x_min = self.x_axis_config["v_min"]
        self.x_max = self.x_axis_config["v_max"]
        self.y_min = self.y_axis_config["v_min"]
        self.y_max = self.y_axis_config["v_max"]

        self.add(self.x_axis,self.y_axis)

    def get_dots_from_coords(self,coords,**dots_kwargs):
        points = self.get_points_from_coords(coords)
        dots = VGroup(*[
            Dot(**dots_kwargs).move_to([px,py,pz])
            for px,py,pz in points
            ]
        )
        return dots


    def get_fixed_axis(self,direction,**config):
        num_range = float(config["v_max"] - config["v_min"])
        config["unit_size"] = (config["axes_size"] / num_range) * config["zoom"]
        if config["avoids_normalization"]:
            if config["max_size"] == None:
                config["max_size"] = config["v_max"] 
            config["v_max"] = config["max_size"] 
        config["show_line"] = self.show_lines
        if direction == "Y":
            config["label_direction"] = LEFT
        axis = DataAxis(**config)
        axis.shift(self.graph_origin - axis.number_to_point(0))
        if direction == "Y":
            axis.rotate(np.pi / 2, about_point=axis.number_to_point(0))
        axis.add_numbers()
        if config["fade_alpha_size"] > 0:
            self.fade_parts(axis,config["fade_alpha_size"],config["fade_alpha_shift"])
        return axis

    def fade_parts(self,axis,f_size,f_shift,v_min=None,v_max=None):
        axis.fade_parts(
            f_size,
            axis.get_coord_v_min(),
            axis.get_coord_v_max(),
            f_shift
        )

    def fade_x_axis_parts_again(self):
        self.x_axis.fade_parts(
            self.x_axis_config["fade_alpha_size"],
            self.x_axis.get_coord_v_min(),
            self.x_axis.get_coord_v_max(),
            self.x_axis_config["fade_alpha_shift"],
        )

    def fade_y_axis_parts_again(self):
        self.y_axis.fade_parts(
            self.y_axis_config["fade_alpha_size"],
            self.y_axis.get_coord_v_min(),
            self.y_axis.get_coord_v_max(),
            self.y_axis_config["fade_alpha_shift"],
        )

    def fade_axes_parts_again(self):
        self.fade_x_axis_parts_again()
        self.fade_y_axis_parts_again()

    def get_x_unit_size(self):
        return self.x_axis.unit_size

    def get_y_unit_size(self):
        return self.y_axis.unit_size

    def coords_to_point(self, x, y):
        assert(hasattr(self, "x_axis") and hasattr(self, "y_axis"))
        result = self.x_axis.number_to_point(x)[0] * RIGHT
        result += self.y_axis.number_to_point(y)[1] * UP
        return result

    def point_to_coords(self, point):
        return (self.x_axis.point_to_number(point),
                self.y_axis.point_to_number(point))

    def get_graph(self, func,color=None,x_min=None,x_max=None,**kwargs):
        if color is None:
            color = WHITE
        if x_min is None:
            x_min = self.x_min
        if x_max is None:
            x_max = self.x_max

        def parameterized_function(alpha):
            x = interpolate(x_min, x_max, alpha)
            y = func(x)
            if not np.isfinite(y):
                y = self.y_max
            return self.coords_to_point(x, y)

        graph = ParametricFunction(
            parameterized_function,
            color=color,
            **kwargs
        )
        graph.underlying_function = func
        return graph

    def get_graph_from_data(self,
                            file,
                            start=None,
                            end=None,
                            shift=0,
                            pre_seconds=0,
                            dt=0.02,
                            type="dots",
                            partitioned=True,
                            graph_kwargs={}):
        if not start:
            start = self.x_axis_config["v_min"]
        if not end:
            end = self.x_axis_config["v_max"]
        raw_coords = self.get_coords_from_csv_raw(file,pre_seconds,dt)
        coords = self.get_coords_bounded(raw_coords,start,end,shift)
        points = self.get_points_from_coords(coords)
        if type == "line":
            graph = SmoothGraphFromSetPoints(points,**graph_kwargs)
            if partitioned:
                graph = CurvesAsSubmobjects(graph)
        elif type == "dots":
            graph = self.get_dots_from_coords(coords,**graph_kwargs)
        return graph

    def get_coords_from_csv_raw(self,file_name,pre_seconds=0,dt=0.02):
        if pre_seconds != 0:
            div = 1 / dt
            limit = int(- pre_seconds * div)
            rang = range(limit,0)
            coords = [np.array([x / div , 0]) for x in rang]
        else:
            coords = []

        with open(f'{file_name}.csv', 'r') as csvFile:
            reader = csv.reader(csvFile)
            for row in reader:
                x,y = row
                coord = [float(x),float(y)]
                coords.append(coord)
        csvFile.close()
        return coords

    def get_coords_bounded(self,coords,start,end,shift=0):
        coords_fixed = []
        shift_x_min = start + shift
        shift_x_max = end + shift
        for x,y in coords:
            if shift_x_min <= x <= shift_x_max:
                coords_fixed.append([x - shift,y])
        return coords_fixed

    def get_points_from_coords(self,coords):
        return [
            self.coords_to_point(px,py)
            for px,py in coords
        ]

    def get_all_parts(self):
        return VGroup(
            *self.x_axis.get_all_parts(),
            *self.y_axis.get_all_parts()
        )

    def get_all_x_parts(self):
        return VGroup(*self.x_axis.get_all_parts())

    def get_all_y_parts(self):
        return VGroup(*self.y_axis.get_all_parts())

    def get_all_x_fade_parts(self):
        return VGroup(*self.x_axis.get_all_fade_parts())

    def get_all_y_fade_parts(self):
        return VGroup(*self.y_axis.get_all_fade_parts())

    def get_coord_x_min(self):
        return self.coords_to_point(self.x_min,0)[0]

    def get_coord_x_max(self):
        return self.coords_to_point(self.x_max,0)[0]

    def get_coord_y_min(self):
        return self.coords_to_point(0,self.y_min)[1]

    def get_coord_y_max(self):
        return self.coords_to_point(0,self.y_max)[1]