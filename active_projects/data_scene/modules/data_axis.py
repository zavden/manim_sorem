from manimlib.constants import *
from manimlib.mobject.types.vectorized_mobject import VMobject
from manimlib.mobject.types.vectorized_mobject import VGroup
from manimlib.utils.config_ops import digest_config
from manimlib.utils.bezier import interpolate
from active_projects.data_scene.modules.number_text_line import NumberTextLine

class DataAxis(VMobject):
    CONFIG = {
        "v_max": 0.5,
        "v_min": -0.3,
        "number_frequency": 0.1,
        "min_number": None,
        "max_number": None,
        "unit_size": 8,
        "decimal_number_config": {
            "num_decimal_places": 1,
            "unit": "u",
            "unit_config": {
                "color": WHITE
            }
        },
        "include_numbers": False,
        "line_partitions": 0,
        "label_direction": DOWN,
        "show_thiks": True,
        "show_line": True,
        "line_config": {},
        "tick_marks_config": {},
        "tick_size": 0.1,
        "line_to_number_buff": MED_SMALL_BUFF,
        "min_scale_to_show": 0,
        "center_tick": 0,
        "fade_alpha_size": 2,
        "fade_alpha_shift": 0,
        "number_scale_val": 0.4,
    }
    def __init__(self,**kwargs):
        digest_config(self, kwargs)
        super().__init__()
        max_number = self.max_number if self.max_number != None else self.v_max
        self.list_numbers_to_show = list(
            np.arange(
                    self.min_number if self.min_number != None else self.v_min,
                    max_number + self.number_frequency,
                    self.number_frequency
            )
        )
        if self.list_numbers_to_show[-1] > max_number:
            self.list_numbers_to_show.pop()
        self.numberline = NumberTextLine(
            x_min=self.v_min,
            x_max=self.v_max,
            leftmost_tick=self.v_min,
            unit_size=self.unit_size,
            #include_numbers=self.include_numbers,
            label_direction=self.label_direction,
            numbers_to_show=self.list_numbers_to_show,
            tick_frequency=self.number_frequency,
            decimal_number_config=self.decimal_number_config,
            line_partitions=self.line_partitions,
            line_config=self.line_config,
            tick_marks_config=self.tick_marks_config,
            tick_size=self.tick_size,
            line_to_number_buff=self.line_to_number_buff,
            min_scale_to_show=self.min_scale_to_show,
            center_tick=self.center_tick,
            number_scale_val=self.number_scale_val,
        )
        self.add(*self.numberline)
        self.width_of_ticks = self.get_height()
        if self.include_numbers:
            self.numberline.add_numbers(*self.list_numbers_to_show)
            self.add(*self.numberline.get_numbers())
        if not self.show_thiks:
            self.numberline.all_tick_marks.set_stroke(opacity=0)
        if not self.show_line:
            line = self.numberline.get_line()
            line.set_stroke(opacity=0,color=RED)
            

    def get_numbers(self):
        return self.numberline.numbers

    def get_tick_marks(self):
        return VGroup(
            *self.numberline.tick_marks,
            *self.numberline.big_tick_marks
        )

    def get_line(self):
        if self.numberline.line_partitions > 1:
            return self.numberline.line_as_submobjects
        else:
            return self.numberline.line

    def number_to_point(self,number):
        return self.numberline.number_to_point(number)

    def point_to_number(self,point):
        return self.numberline.point_to_number(point)

    def get_all_parts(self):
        return self.numberline.get_all_parts()

    def get_all_fade_parts(self):
        return self.numberline.get_all_fade_parts()

    def add_numbers(self):
        self.numberline.add_numbers(*self.list_numbers_to_show)
        self.add(*self.numberline.get_numbers())

    def get_fade_ticks(self):
        return self.numberline.get_fade_ticks()

    def get_unit_vector(self):
        return self.numberline.line.get_unit_vector()

    def approx_ticks(self,x,y,dt):
        return True if abs(x-y) < dt else False

    def check_direction_index(self,vector):
        approx = self.approx_ticks(vector[1],0,0.0000001)
        return 0 if approx else 1

    def fix_array(self,start,end,dt):
        final = []
        for x in start:
            for i in end:
                if self.approx_ticks(x,i,dt):
                    final.append(i)
        return final

    def get_center_tick(self):
        return self.center_tick

    def get_coord(self,coord):
        unit_vector = self.get_unit_vector()
        index = self.check_direction_index(unit_vector)
        return self.number_to_point(coord)[index]

    def get_coord_v_min(self):
        return self.get_coord(self.v_min)

    def get_coord_v_max(self):
        return self.get_coord(self.v_max)

    def fade_parts(
                    self,
                    fade_alpha_size,
                    start_fade,
                    end_fade,
                    shift=1,
                    direction=1):
        parts = VGroup(*self.get_numbers(),*self.get_tick_marks())
        vector = self.get_unit_vector() * direction
        index = self.check_direction_index(vector)
        fade_ticks = self.get_fade_ticks()
        #---------------------- FADE EXTREMES
        start_fade -= shift
        end_fade   += shift
        end_i   = start_fade
        start_i = (start_fade + fade_alpha_size)
        end_f   = (end_fade - fade_alpha_size)
        start_f = end_fade
        for mob in parts:
            also_fill = True if mob.__class__.__name__ == "DecimalTextNumber" else False
            coord = mob.get_coord(index)
            for coord_f in fade_ticks:
                coord_f = self.number_to_point(coord_f)[index]
                if self.approx_ticks(coord_f,coord,0.00001):
                    #print("aprox: ",coord_f)
                    mob.fade(1)
                    break
                else:
                    #print("fade: ",coord_f)
                    if end_f <= coord <= start_f:
                        alpha = abs( (start_f - coord) / (end_f - start_f) )
                        fade = interpolate(0,1,alpha)
                        if also_fill:
                            mob.set_fill(opacity=fade)
                        mob.set_style(stroke_opacity=fade)
                    elif start_i < coord < end_f:
                        if also_fill:
                            mob.set_fill(opacity=1)
                        mob.set_style(stroke_opacity=1)
                    elif end_i <= coord <= start_i:
                        alpha = abs( (start_i - coord) / (end_i - start_i) ) 
                        fade = interpolate(0,1,alpha)
                        if also_fill:
                            mob.set_fill(opacity=1-fade)
                        mob.set_style(stroke_opacity=1-fade)
                    else:
                        if also_fill:
                            mob.set_fill(opacity=0)
                        mob.set_style(stroke_opacity=0)
        #----------------------------
        #Fade small ticks