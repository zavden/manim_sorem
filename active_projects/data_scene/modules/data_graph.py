import itertools as it
import csv
from manimlib.animation.creation import Write, DrawBorderThenFill, ShowCreation
from manimlib.animation.transform import Transform
from manimlib.animation.update import UpdateFromAlphaFunc
from manimlib.constants import *
from manimlib.mobject.functions import ParametricFunction
from manimlib.mobject.geometry import Line
from manimlib.mobject.geometry import Rectangle
from manimlib.mobject.geometry import RegularPolygon
from manimlib.mobject.number_line import NumberLine
from manimlib.mobject.svg.tex_mobject import TexMobject
from manimlib.mobject.svg.tex_mobject import TextMobject
from manimlib.mobject.types.vectorized_mobject import VGroup
from manimlib.mobject.types.vectorized_mobject import VMobject
from manimlib.mobject.types.vectorized_mobject import CurvesAsSubmobjects
from manimlib.mobject.types.vectorized_mobject import VectorizedPoint
from manimlib.scene.scene import Scene
from manimlib.utils.bezier import interpolate
from manimlib.utils.color import color_gradient
from manimlib.utils.color import invert_color
from manimlib.utils.space_ops import angle_of_vector
from manimlib.utils.config_ops import merge_dicts_recursively
# NEW
from active_projects.data_scene.modules.decimal_text_number import *
from active_projects.data_scene.modules.number_text_line import *
from active_projects.data_scene.modules.screen_grid import ScreenGrid
from manimlib.camera.moving_camera import MovingCamera
from manimlib.utils.iterables import list_update

# TODO, this should probably reimplemented entirely, especially so as to
# better reuse code from mobject/coordinate_systems.
# Also, I really dislike how the configuration is set up, this
# is way too messy to work with.

def get_coords_from_csv_fixed(file_name,pre_seconds=0,dt=0.02):
    if pre_seconds != 0:
        div = 1 / dt
        coords = [np.array([x / div , 0]) for x in range(int(- pre_seconds * div),0)]
    else:
        coords = []

    with open(f'{file_name}.csv', 'r') as csvFile:
        reader = csv.reader(csvFile)
        for row in reader:
            x,y = row
            coord = [float(x),float(y)]
            coords.append(coord)
    csvFile.close()
    return coords

def get_coords_fixed(coords,start,end,shift=0):
    coords_fixed = []
    for coord in coords:
        shift_x_min = start + shift
        shift_x_max = end + shift
        if shift_x_min <= coord[0] <= shift_x_max:
            coords_fixed.append([coord[0]-shift,coord[1]])
    return coords_fixed

class DiscreteGraphFromSetPoints(VMobject):
    def __init__(self,set_of_points,**kwargs):
        super().__init__(**kwargs)
        self.set_points_as_corners(set_of_points)

class SmoothGraphFromSetPoints(VMobject):
    def __init__(self,set_of_points,**kwargs):
        super().__init__(**kwargs)
        self.set_points_smoothly(set_of_points)


class DataGraphScene(Scene):
    CONFIG = {
        "camera_class": MovingCamera,
        "include_grid": True,
        "grid_config": {},
        "x_initial_config": {
            "v_min": 0, # <- No modify
            "v_max": 10,
            "axis_size": 10,
            "tick_frequency": 1,
            "labeled_nums": None,
            "axis_label": "$x$",
            "direction": "X",
            "label_position": None,
            "tick_size": 0,
            "decimal_config": {},
        },
        "y_initial_config": {
            "v_min": -1,
            "v_max": 1,
            "axis_size": 6,
            "tick_frequency": 0.2,
            "labeled_nums": list(np.arange(-1,1.2,0.2)),
            "axis_label": "$y$",
            "label_position": None,
            "direction": "Y",
            "tick_size": 0.2,
            "decimal_config": {
                "unit": "V",
                "num_decimal_places":1
            },
        },
        "axes_color": GREY,
        "graph_origin": 4 * LEFT,
        "exclude_zero_label": True,
        "default_graph_colors": [BLUE, GREEN, YELLOW],
        "default_derivative_color": GREEN,
        "default_input_color": YELLOW,
        "area_opacity": 0.8,
        "num_rects": 50,
    }

    def setup(self):
        Scene.setup(self)
        assert(isinstance(self.camera, MovingCamera))
        self.camera_frame = self.camera.frame

        self.default_graph_colors_cycle = it.cycle(self.default_graph_colors)
        self.graph_grid = ScreenGrid(**self.grid_config)
        if self.include_grid:
            self.add(self.graph_grid)
        return self

    def get_axis(self,
                 v_min,
                 v_max,
                 axis_size, #axis_width
                 direction="X",
                 labeled_nums=None,
                 leftmost_tick=None,
                 tick_frequency=1,
                 decimal_config={},
                 label_scale=0.4,
                 label_position=None,
                 exclude_zero_label=True,
                 line_to_number_buff=0.3,
                 tick_size=0.2,
                 axis_label=None):
        num_range = float(v_max - v_min)
        space_unit_to = axis_size / num_range
        if labeled_nums is None:
            labeled_nums = []
        if leftmost_tick is None:
            leftmost_tick = v_min
        if direction == "Y":
            label_direction = LEFT
        else:
            label_direction = None
        axis = NumberTextLine(
            x_min=v_min,
            x_max=v_max,
            unit_size=space_unit_to,
            tick_frequency=tick_frequency,
            leftmost_tick=leftmost_tick,
            numbers_to_show=labeled_nums,
            tick_size=tick_size,
            color=self.axes_color,
            decimal_number_config=decimal_config,
            line_to_number_buff=line_to_number_buff,
            number_scale_val=label_scale,
            label_direction=label_direction,
        )
        axis.shift(self.graph_origin - axis.number_to_point(0))
        if direction == "Y":
            axis.rotate(np.pi / 2, about_point=axis.number_to_point(0))
        if len(labeled_nums) > 0:
            if exclude_zero_label:
                labeled_nums = [x for x in labeled_nums if x != 0]
            axis.add_numbers(*labeled_nums)
        if axis_label:
            label = TextMobject(axis_label)
            label.next_to(
                axis.get_tick_marks(), UP + RIGHT,
                buff=SMALL_BUFF
            )
            label.shift_onto_screen()
            axis.add(label)
            if label_position:
                label.move_to(label_position)
        #else:
        #    axis[3].fade(0)
        return axis

    def setup_axes(self, animate=False):
        x_axis = self.get_axis(**self.x_initial_config)
        y_axis = self.get_axis(**self.y_initial_config)
        if animate:
            self.play(Write(VGroup(x_axis, y_axis)))
        else:
            self.add(x_axis, y_axis)
        self.x_base_axis, self.y_base_axis = VGroup(x_axis, y_axis)
        self.default_graph_colors = it.cycle(self.default_graph_colors)

    def coords_to_point(self, x, y):
        assert(hasattr(self, "x_base_axis") and hasattr(self, "y_base_axis"))
        result = self.x_base_axis.number_to_point(x)[0] * RIGHT
        result += self.y_base_axis.number_to_point(y)[1] * UP
        return result

    def point_to_coords(self, point):
        return (self.x_base_axis.point_to_number(point),
                self.y_base_axis.point_to_number(point))

    def get_graph(
        self, func,
        color=None,
        x_min=None,
        x_max=None,
        **kwargs
    ):
        if color is None:
            color = next(self.default_graph_colors_cycle)
        if x_min is None:
            x_min = self.x_initial_config["v_min"]
        if x_max is None:
            x_max = self.x_initial_config["v_max"]

        def parameterized_function(alpha):
            x = interpolate(x_min, x_max, alpha)
            y = func(x)
            if not np.isfinite(y):
                y = self.y_initial_config["v_max"]
            return self.coords_to_point(x, y)

        graph = ParametricFunction(
            parameterized_function,
            color=color,
            **kwargs
        )
        graph.underlying_function = func
        return graph

    def get_graph_from_data(self,
                            coords,
                            start=None,
                            end=None,
                            shift=0,
                            pre_seconds=0,
                            dt=0.02,
                            **kwargs):
        if not start:
            start = self.x_initial_config["v_min"]
        if not end:
            end = self.x_initial_config["v_max"]
        #pre_coords = get_coords_from_csv_fixed(file,pre_seconds,dt)
        coords = get_coords_fixed(coords,start,end,shift)
        #print(coords)
        points = self.get_points_from_coords(coords)
        graph = SmoothGraphFromSetPoints(points,**kwargs)
        return graph

    def fade_graph_extremes(self,graph,p_fade=0.3):
        if p_fade != None:
            plot_c = CurvesAsSubmobjects(graph)
            cant_sub_fade = int(p_fade * len(plot_c))
            for i in range(cant_sub_fade):
                alpha = i / cant_sub_fade
                fade = interpolate(0,1,alpha)
                plot_c[i].fade(1 - fade)
                plot_c[len(plot_c) - cant_sub_fade + i].fade(fade)
            return plot_c
        else:
            return graph


    def get_points_from_coords(self,coords):
        return [
            # Convert COORDS -> POINTS
            self.coords_to_point(px,py)
            # See manimlib/scene/graph_scene.py
            for px,py in coords
        ]

    # Return the dots of a set of points
    def get_dots_from_coords(self,coords,radius=0.1):
        points = self.get_points_from_coords(coords)
        dots = VGroup(*[
            Dot(radius=radius).move_to([px,py,pz])
            for px,py,pz in points
            ]
        )
        return dots


    def input_to_graph_point(self, x, graph):
        return self.coords_to_point(x, graph.underlying_function(x))

    def angle_of_tangent(self, x, graph, dx=0.01):
        vect = self.input_to_graph_point(
            x + dx, graph) - self.input_to_graph_point(x, graph)
        return angle_of_vector(vect)

    def slope_of_tangent(self, *args, **kwargs):
        return np.tan(self.angle_of_tangent(*args, **kwargs))

    def get_derivative_graph(self, graph, dx=0.01, **kwargs):
        if "color" not in kwargs:
            kwargs["color"] = self.default_derivative_color

        def deriv(x):
            return self.slope_of_tangent(x, graph, dx) / self.space_unit_to_y
        return self.get_graph(deriv, **kwargs)

    def get_graph_label(
        self,
        graph,
        label="f(x)",
        x_val=None,
        direction=RIGHT,
        buff=MED_SMALL_BUFF,
        color=None,
    ):
        label = TexMobject(label)
        color = color or graph.get_color()
        label.set_color(color)
        if x_val is None:
            # Search from right to left
            for x in np.linspace(self.x_initial_config["v_max"], self.x_initial_config["v_min"], 100):
                point = self.input_to_graph_point(x, graph)
                if point[1] < FRAME_Y_RADIUS:
                    break
            x_val = x
        label.next_to(
            self.input_to_graph_point(x_val, graph),
            direction,
            buff=buff
        )
        label.shift_onto_screen()
        return label



    def get_vertical_line_to_graph(
        self,
        x, graph,
        line_class=Line,
        **line_kwargs
    ):
        if "color" not in line_kwargs:
            line_kwargs["color"] = graph.get_color()
        return line_class(
            self.coords_to_point(x, 0),
            self.input_to_graph_point(x, graph),
            **line_kwargs
        )

    def get_vertical_lines_to_graph(
        self, graph,
        x_min=None,
        x_max=None,
        num_lines=20,
        **kwargs
    ):
        x_min = x_min or self.x_initial_config["v_min"]
        x_max = x_max or self.x_initial_config["v_max"]
        return VGroup(*[
            self.get_vertical_line_to_graph(x, graph, **kwargs)
            for x in np.linspace(x_min, x_max, num_lines)
        ])

    def get_moving_mobjects(self, *animations):
        moving_mobjects = Scene.get_moving_mobjects(self, *animations)
        all_moving_mobjects = self.camera.extract_mobject_family_members(
            moving_mobjects
        )
        movement_indicators = self.camera.get_mobjects_indicating_movement()
        for movement_indicator in movement_indicators:
            if movement_indicator in all_moving_mobjects:
                # When one of these is moving, the camera should
                # consider all mobjects to be moving
                return list_update(self.mobjects, moving_mobjects)
        return moving_mobjects


#BACK
class DataGraphScene2(Scene):
    CONFIG = {
        "camera_class": MovingCamera,
        "x_min": 0,
        "x_max": 10,
        "x_axis_width": 9,
        "x_tick_frequency": 1,
        "x_leftmost_tick": None,  # Change if different from x_min
        "x_labeled_nums": None,
        "x_axis_label": "$x$",
        "y_min": -1,
        "y_max": 10,
        "y_axis_height": 6,
        "y_tick_frequency": 1,
        "y_bottom_tick": None,  # Change if different from y_min
        "y_labeled_nums": None,
        "y_axis_label": "$y$",
        "axes_color": GREY,
        "graph_origin": 2.5 * DOWN + 4 * LEFT,
        "exclude_zero_label": True,
        "default_graph_colors": [BLUE, GREEN, YELLOW],
        "default_derivative_color": GREEN,
        "default_input_color": YELLOW,
        "default_riemann_start_color": BLUE,
        "default_riemann_end_color": GREEN,
        "area_opacity": 0.8,
        "num_rects": 50,
        "x_decimal_config": {},
        "y_decimal_config": {},
        "x_label_scale": 0.4,
        "y_label_scale": 0.4,
    }

    def setup(self):
        Scene.setup(self)
        assert(isinstance(self.camera, MovingCamera))
        self.camera_frame = self.camera.frame

        self.default_graph_colors_cycle = it.cycle(self.default_graph_colors)
        self.left_T_label = VGroup()
        self.left_v_line = VGroup()
        self.right_T_label = VGroup()
        self.right_v_line = VGroup()
        return self

    def setup_axes(self, animate=False):
        # TODO, once eoc is done, refactor this to be less redundant.
        x_num_range = float(self.x_max - self.x_min)
        self.space_unit_to_x = self.x_axis_width / x_num_range
        if self.x_labeled_nums is None:
            self.x_labeled_nums = []
        if self.x_leftmost_tick is None:
            self.x_leftmost_tick = self.x_min
        x_axis = NumberTextLine(
            x_min=self.x_min,
            x_max=self.x_max,
            unit_size=self.space_unit_to_x,
            tick_frequency=self.x_tick_frequency,
            leftmost_tick=self.x_leftmost_tick,
            numbers_with_elongated_ticks=self.x_labeled_nums,
            color=self.axes_color,
            decimal_number_config=self.x_decimal_config,
            number_scale_val=self.x_label_scale,
        )
        x_axis.shift(self.graph_origin - x_axis.number_to_point(0))
        if len(self.x_labeled_nums) > 0:
            if self.exclude_zero_label:
                self.x_labeled_nums = [x for x in self.x_labeled_nums if x != 0]
            x_axis.add_numbers(*self.x_labeled_nums)
        if self.x_axis_label:
            x_label = TextMobject(self.x_axis_label)
            x_label.next_to(
                x_axis.get_tick_marks(), UP + RIGHT,
                buff=SMALL_BUFF
            )
            x_label.shift_onto_screen()
            x_axis.add(x_label)
            self.x_axis_label_mob = x_label

        y_num_range = float(self.y_max - self.y_min)
        self.space_unit_to_y = self.y_axis_height / y_num_range

        if self.y_labeled_nums is None:
            self.y_labeled_nums = []
        if self.y_bottom_tick is None:
            self.y_bottom_tick = self.y_min
        y_axis = NumberTextLine(
            x_min=self.y_min,
            x_max=self.y_max,
            unit_size=self.space_unit_to_y,
            tick_frequency=self.y_tick_frequency,
            leftmost_tick=self.y_bottom_tick,
            numbers_with_elongated_ticks=self.y_labeled_nums,
            color=self.axes_color,
            line_to_number_vect=LEFT,
            label_direction=LEFT,
            decimal_number_config=self.y_decimal_config,
            number_scale_val=self.y_label_scale,
        )
        y_axis.shift(self.graph_origin - y_axis.number_to_point(0))
        y_axis.rotate(np.pi / 2, about_point=y_axis.number_to_point(0))
        if len(self.y_labeled_nums) > 0:
            if self.exclude_zero_label:
                self.y_labeled_nums = [y for y in self.y_labeled_nums if y != 0]
            y_axis.add_numbers(*self.y_labeled_nums)
        if self.y_axis_label:
            y_label = TextMobject(self.y_axis_label)
            y_label.next_to(
                y_axis.get_corner(UP + RIGHT), UP + RIGHT,
                buff=SMALL_BUFF
            )
            y_label.shift_onto_screen()
            y_axis.add(y_label)
            self.y_axis_label_mob = y_label

        if animate:
            self.play(Write(VGroup(x_axis, y_axis)))
        else:
            self.add(x_axis, y_axis)
        self.x_axis, self.y_axis = self.axes = VGroup(x_axis, y_axis)
        self.default_graph_colors = it.cycle(self.default_graph_colors)

    def coords_to_point(self, x, y):
        assert(hasattr(self, "x_axis") and hasattr(self, "y_axis"))
        result = self.x_axis.number_to_point(x)[0] * RIGHT
        result += self.y_axis.number_to_point(y)[1] * UP
        return result

    def point_to_coords(self, point):
        return (self.x_axis.point_to_number(point),
                self.y_axis.point_to_number(point))

    def get_graph(
        self, func,
        color=None,
        x_min=None,
        x_max=None,
        **kwargs
    ):
        if color is None:
            color = next(self.default_graph_colors_cycle)
        if x_min is None:
            x_min = self.x_min
        if x_max is None:
            x_max = self.x_max

        def parameterized_function(alpha):
            x = interpolate(x_min, x_max, alpha)
            y = func(x)
            if not np.isfinite(y):
                y = self.y_max
            return self.coords_to_point(x, y)

        graph = ParametricFunction(
            parameterized_function,
            color=color,
            **kwargs
        )
        graph.underlying_function = func
        return graph

    def get_graph_from_data(self,
                            coords,
                            start=None,
                            end=None,
                            shift=0,
                            pre_seconds=0,
                            dt=0.02,
                            p_fade=0.3,
                            **kwargs):
        if not start:
            start = self.x_min
        if not end:
            end = self.x_max
        #pre_coords = get_coords_from_csv_fixed(file,pre_seconds,dt)
        coords = get_coords_fixed(coords,start,end,shift)
        #print(coords)
        points = self.get_points_from_coords(coords)
        graph = SmoothGraphFromSetPoints(points,**kwargs)
        if p_fade != None:
            plot_c = CurvesAsSubmobjects(graph)
            cant_sub_fade = int(p_fade * len(plot_c))
            for i in range(cant_sub_fade):
                alpha = i / cant_sub_fade
                fade = interpolate(0,1,alpha)
                plot_c[i].fade(1 - fade)
                plot_c[len(plot_c) - cant_sub_fade + i].fade(fade)
            return plot_c
        else:
            return graph

    def get_points_from_coords(self,coords):
        return [
            # Convert COORDS -> POINTS
            self.coords_to_point(px,py)
            # See manimlib/scene/graph_scene.py
            for px,py in coords
        ]

    # Return the dots of a set of points
    def get_dots_from_coords(self,coords,radius=0.1):
        points = self.get_points_from_coords(coords)
        dots = VGroup(*[
            Dot(radius=radius).move_to([px,py,pz])
            for px,py,pz in points
            ]
        )
        return dots


    def input_to_graph_point(self, x, graph):
        return self.coords_to_point(x, graph.underlying_function(x))

    def angle_of_tangent(self, x, graph, dx=0.01):
        vect = self.input_to_graph_point(
            x + dx, graph) - self.input_to_graph_point(x, graph)
        return angle_of_vector(vect)

    def slope_of_tangent(self, *args, **kwargs):
        return np.tan(self.angle_of_tangent(*args, **kwargs))

    def get_derivative_graph(self, graph, dx=0.01, **kwargs):
        if "color" not in kwargs:
            kwargs["color"] = self.default_derivative_color

        def deriv(x):
            return self.slope_of_tangent(x, graph, dx) / self.space_unit_to_y
        return self.get_graph(deriv, **kwargs)

    def get_graph_label(
        self,
        graph,
        label="f(x)",
        x_val=None,
        direction=RIGHT,
        buff=MED_SMALL_BUFF,
        color=None,
    ):
        label = TexMobject(label)
        color = color or graph.get_color()
        label.set_color(color)
        if x_val is None:
            # Search from right to left
            for x in np.linspace(self.x_max, self.x_min, 100):
                point = self.input_to_graph_point(x, graph)
                if point[1] < FRAME_Y_RADIUS:
                    break
            x_val = x
        label.next_to(
            self.input_to_graph_point(x_val, graph),
            direction,
            buff=buff
        )
        label.shift_onto_screen()
        return label



    def get_vertical_line_to_graph(
        self,
        x, graph,
        line_class=Line,
        **line_kwargs
    ):
        if "color" not in line_kwargs:
            line_kwargs["color"] = graph.get_color()
        return line_class(
            self.coords_to_point(x, 0),
            self.input_to_graph_point(x, graph),
            **line_kwargs
        )

    def get_vertical_lines_to_graph(
        self, graph,
        x_min=None,
        x_max=None,
        num_lines=20,
        **kwargs
    ):
        x_min = x_min or self.x_min
        x_max = x_max or self.x_max
        return VGroup(*[
            self.get_vertical_line_to_graph(x, graph, **kwargs)
            for x in np.linspace(x_min, x_max, num_lines)
        ])

    def get_moving_mobjects(self, *animations):
        moving_mobjects = Scene.get_moving_mobjects(self, *animations)
        all_moving_mobjects = self.camera.extract_mobject_family_members(
            moving_mobjects
        )
        movement_indicators = self.camera.get_mobjects_indicating_movement()
        for movement_indicator in movement_indicators:
            if movement_indicator in all_moving_mobjects:
                # When one of these is moving, the camera should
                # consider all mobjects to be moving
                return list_update(self.mobjects, moving_mobjects)
        return moving_mobjects
