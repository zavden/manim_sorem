import operator as op

from manimlib.constants import *
from manimlib.mobject.geometry import Line
from manimlib.mobject.types.vectorized_mobject import VGroup
from manimlib.mobject.types.vectorized_mobject import VMobject
from manimlib.mobject.types.vectorized_mobject import CurvesAsSubmobjects
from manimlib.utils.bezier import interpolate
from manimlib.utils.config_ops import digest_config
from manimlib.utils.config_ops import merge_dicts_recursively
from manimlib.utils.simple_functions import fdiv
from manimlib.utils.space_ops import normalize,get_norm
from manimlib.mobject.svg.text_mobject import Text
from active_projects.data_scene.modules.decimal_text_number import DecimalTextNumber


class NumberTextLine(VGroup):
    CONFIG = {
        "x_min": -FRAME_X_RADIUS,
        "x_max": FRAME_X_RADIUS,
        "unit_size": 1,
        "include_ticks": True,
        "tick_size": 0.1,
        "tick_frequency": 1,
        # Defaults to value near x_min s.t. 0 is a tick
        # TODO, rename this
        "leftmost_tick": None,
        "min_scale_to_show": 0,
        "center_tick": 0,
        # Change name
        "numbers_with_elongated_ticks": [0],
        "include_numbers": False,
        "numbers_to_show": None,
        "longer_tick_multiple": 1,
        "number_at_center": 0,
        "number_scale_val": 0.4,
        "label_direction": DOWN,
        "line_to_number_buff": MED_SMALL_BUFF,
        "include_tip": False,
        "tip_width": 0.25,
        "tip_height": 0.25,
        "decimal_number_config": {
            "num_decimal_places": 0,
        },
        "exclude_zero_from_default_numbers": False,
        "line_partitions": 0,
        "line_config": {
            "color":LIGHT_GREY,
            "background_stroke_opacity": 0,
        },
        "tick_marks_config": {
            "color":LIGHT_GREY,
            "background_stroke_opacity": 0,
        },
        "size_fade_transition": None,
    }

    def __init__(self, **kwargs):
        digest_config(self, kwargs)
        super().__init__(**kwargs)
        start = self.unit_size * self.x_min * RIGHT
        end = self.unit_size * self.x_max * RIGHT
        self.line = Line(start, end, **self.line_config)
        self.add(self.line)
        if self.line_partitions > 1:
            self.line.set_stroke(opacity=0)
            step = self.line.get_length() / self.line_partitions
            self.line_as_submobjects = VGroup(*[
                Line(
                    start + i * step * RIGHT,
                    start + (i + 1) * step * RIGHT,
                    **self.line_config
                )
                for i in range(self.line_partitions)
            ])
            self.add(self.line_as_submobjects)

        self.shift(-self.number_to_point(self.number_at_center))
        self.upper_ticks = []
        self.lower_ticks = []
        self.fade_ticks = []
        self.init_leftmost_tick()
        if self.include_tip:
            self.add_tip()
        if self.include_ticks:
            self.add_tick_marks()
        if self.include_numbers:
            self.add_numbers()
        if self.size_fade_transition != None:
            self.fade_numberline_extremes()

    def get_line(self):
        return self.line

    def get_line_direction(self):
        return self.line.get_unit_vector()

    def init_leftmost_tick(self):
        if self.leftmost_tick is None:
            self.leftmost_tick = op.mul(
                self.tick_frequency,
                np.ceil(self.x_min / self.tick_frequency)
            )

    def approx_ticks(self,x,y,dt):
        if abs(x-y) < dt:
            return True
        else:
            return False

    def fix_array(self,start,end,dt):
        final = []
        for x in start:
            for i in end:
                if self.approx_ticks(x,i,dt):
                    final.append(i)
        return final

    def add_tick_marks(self):
        tick_size = self.tick_size
        self.partition_ticks()
        self.set_fade_ticks(1)
        self.set_fade_ticks(-1)
        self.tick_marks = VGroup()
        tick_numbers = self.get_tick_numbers()
        #self.fade_ticks = self.fix_array(
        #    self.fade_ticks,
        #    tick_numbers,
        #    0.000001
        #)
        #print("-"*30)
        #print("vect: ", self.get_line_direction())
        #print("ticks: ",tick_numbers)
        #print("fades: ",self.fade_ticks)
        #print("-"*30)
        for i in tick_numbers:
            tick = self.get_tick(i, tick_size)
            for x in self.fade_ticks:
                if self.approx_ticks(x,i,0.0000001):
                    tick.fade(1)
            self.tick_marks.add(tick)
                    
        big_tick_size = tick_size * self.longer_tick_multiple
        self.big_tick_marks = VGroup()
        for x in self.numbers_with_elongated_ticks:
            tick = self.get_tick(x, big_tick_size)
            if x in self.fade_ticks:
                tick.fade(1)
            self.big_tick_marks.add(tick)
            
        self.all_tick_marks = VGroup(self.tick_marks,self.big_tick_marks)
        self.add(self.all_tick_marks)

    def get_tick(self, x, size=None):
        if size is None:
            size = self.tick_size
        result = Line(size * DOWN, size * UP, **self.tick_marks_config)
        result.rotate(self.line.get_angle())
        result.move_to(self.number_to_point(x))
        return result

    def get_tick_marks(self):
        return VGroup(
            *self.tick_marks,
            *self.big_tick_marks,
        )

    def get_tick_numbers(self):
        u = -1 if self.include_tip else 1
        return list(np.arange(
            self.leftmost_tick,
            self.x_max + u * self.tick_frequency / 2,
            self.tick_frequency
        ))

    def partition_ticks(self):
        tick_numbers = self.get_tick_numbers()
        for i in tick_numbers:
            if i < self.center_tick:
                self.lower_ticks.append(i)
            if i > self.center_tick:
                self.upper_ticks.append(i)

    def get_lower_ticks(self):
        return self.lower_ticks

    def get_upper_ticks(self):
        return self.upper_ticks

    def check_direction_index(self,vector):
        return 0 if vector[0] != 0 else 1

    def set_fade_ticks(self,direction):
        PRINTS = False
        vector = self.get_line_direction() * direction
        index = self.check_direction_index(vector)
        coord_center = self.number_to_point(self.center_tick)[index]
        coord_limit = vector * self.min_scale_to_show
        dt_size = abs(coord_center - coord_limit[index])
        freq_limit = vector * self.tick_frequency
        dt_frequency = abs(coord_center - self.number_to_point(freq_limit[index])[index]) 
        coord_max = self.number_to_point(self.x_max)[index]
        step = coord_center
        if PRINTS:
            print("index: ",index)
            print("coord_center:",coord_center)
            print("dt_size: ",dt_size)
            print("dt_frequency: ",dt_frequency)
        while step <= coord_max:
            sub_step = step
            limit = step + dt_size
            if PRINTS:
                print("start: ",step)
                print("limit:",limit)
            while sub_step < limit - dt_frequency:
                sub_step += dt_frequency
                sub_step_to_number = self.point_to_number(vector * sub_step)
                if PRINTS:
                    print("sub_step: ",sub_step,"\tsub_step_to_number: ",sub_step_to_number)
                self.fade_ticks.append(sub_step_to_number)
            sub_step += dt_frequency
            if PRINTS: 
                print("end_sub_step:",sub_step)
            step = sub_step
        if PRINTS:
            print("-"*30)

    def get_fade_ticks(self):
        return self.fade_ticks
            

    def number_to_point(self, number):
        alpha = float(number - self.x_min) / (self.x_max - self.x_min)
        return interpolate(
            self.line.get_start(), self.line.get_end(), alpha
        )

    def point_to_number(self, point):
        start_point, end_point = self.line.get_start_and_end()
        full_vect = end_point - start_point
        unit_vect = normalize(full_vect)

        def distance_from_start(p):
            return np.dot(p - start_point, unit_vect)

        proportion = fdiv(
            distance_from_start(point),
            distance_from_start(end_point)
        )
        return interpolate(self.x_min, self.x_max, proportion)

    def n2p(self, number):
        """Abbreviation for number_to_point"""
        return self.number_to_point(number)

    def p2n(self, point):
        """Abbreviation for point_to_number"""
        return self.point_to_number(point)

    def get_unit_size(self):
        return (self.x_max - self.x_min) / self.line.get_length()

    def default_numbers_to_display(self):
        if self.numbers_to_show is not None:
            return self.numbers_to_show
        numbers = np.arange(
            np.floor(self.leftmost_tick),
            np.ceil(self.x_max),
        )
        if self.exclude_zero_from_default_numbers:
            numbers = numbers[numbers != 0]
        return numbers

    def get_number_mobject(self, number,
                           number_config=None,
                           scale_val=None,
                           direction=None,
                           buff=None):
        number_config = merge_dicts_recursively(
            self.decimal_number_config,
            number_config or {},
        )
        if scale_val is None:
            scale_val = self.number_scale_val
        if direction is None:
            direction = self.label_direction
        buff = buff or self.line_to_number_buff
        num_mob = DecimalTextNumber(number, **number_config)
        for i in self.fade_ticks:
            if self.approx_ticks(number,i,0.000001):
                num_mob.fade(1)
        num_mob.scale(scale_val)
        #"""
        num_mob.next_to(
            self.number_to_point(number),
            direction=direction,
            buff=buff
        )
        #"""
        return num_mob

    def get_number_mobjects(self, *numbers, **kwargs):
        if len(numbers) == 0:
            numbers = self.default_numbers_to_display()
        return VGroup(*[
            self.get_number_mobject(number, **kwargs)
            for number in numbers
        ])

    def get_labels(self):
        return self.get_number_mobjects()

    def add_numbers(self, *numbers, **kwargs):
        #print(self.numbers.get_center())
        self.set_numbers()
        self.add(self.numbers)
        return self

    def set_numbers(self):
        self.numbers = self.get_number_mobjects()

    def get_numbers(self):
        return self.numbers

    def fade_numberline_extremes(self):
        end_i   = self.number_to_point(self.x_min)[0]
        start_i = self.number_to_point(self.x_min + self.size_fade_transition)[0]
        end_f   = self.number_to_point(self.x_max - self.size_fade_transition)[0]
        start_f = self.number_to_point(self.x_max)[0]
        fade_objects = [*self.all_tick_marks[0],*self.all_tick_marks[1],*self.numbers]
        if self.line_partitions > 1:
            fade_objects = [*fade_objects,*self.line_as_submobjects]
        for label in fade_objects:
            coord_x = label.get_x()
            if end_f <= coord_x <= start_f:
                alpha = abs( (start_f - coord_x) / (end_f - start_f) )
                fade = interpolate(0,1,alpha)
                label.set_fill(opacity=fade)
                label.set_stroke(opacity=fade)
            elif start_i < coord_x < end_f:
                label.set_fill(opacity=1)
                label.set_stroke(opacity=1)
            elif end_i <= coord_x <= start_i:
                alpha = abs( (start_i - coord_x) / (end_i - start_i) ) 
                fade = interpolate(0,1,alpha)
                label.set_fill(opacity=1-fade)
                label.set_stroke(opacity=1-fade)
            else:
                label.set_fill(opacity=0)
                label.set_stroke(opacity=0)

    def get_all_parts(self):
        mobs = [*self.tick_marks,*self.big_tick_marks,*self.numbers]
        if self.line_partitions > 1:
            mobs = [*mobs,*self.line_as_submobjects]
        else:
            mobs = [*mobs,*self.line]
        return mobs

    def get_all_fade_parts(self):
        mobs = [*self.tick_marks,*self.big_tick_marks,*self.numbers]
        if self.line_partitions > 1:
            mobs = [*mobs,*self.line_as_submobjects]
        return mobs