from manimlib.imports import *
from active_projects.data_scene.modules.imports import *

class ShiftDataGraph(MovingCameraScene):
    CONFIG = {
        "include_grid": True,
        "dt_data": 0.02,
        "start_point": None,
        "end_point": None,
        "initial_shift": 0,
        "pre_seconds": 0,
        "see_initial_state": True,
        "show_progress_data": True,
        "file": "active_projects/data_scene/data/data",
        "graph_origin": 5 * LEFT + 0 * DOWN,
        "graph_type": "dots",
        "graph_config": {},
        "start_animation": False,
        "x_axis_config": {
            "max_size": 20,
            "v_min": 0,
            "v_max": 8,
            #"min_number": 2,
            "line_to_number_buff": 2
        },
        "y_axis_config": {
            "zoom": 1,
            "v_max": 0.2,
            "v_min": -0.2,
            "number_frequency": 0.05,
            "axes_size": 6,
            "min_scale_to_show":0.95,
            "line_config": {},
            "tick_marks_config": {},
            "decimal_number_config": {
                "num_decimal_places": 2,
                "font_config": {
                    "color": TEAL
                }
            }
        },
        "x_fade_alpha_size": 2,
        "x_fade_alpha_shift": 0,
        "y_fade_alpha_size": 2,
        "y_fade_alpha_shift": 1,
        "grid_config":{}
    }
    def setup(self):
        MovingCameraScene.setup(self)
        self.initial_shift = - self.initial_shift
        if self.include_grid:
            self.add(ScreenGrid(**self.grid_config))
        self.axes_initial_config = {
            "graph_origin": self.graph_origin,
            "x_axis_config": self.x_axis_config,
            "y_axis_config": self.y_axis_config,
        }
        self.axes = DataAxes(**self.axes_initial_config)
        self.x_fixed_axis_full, self.y_fixed_axis_full = self.axes
        self.fixed_axes = VGroup(
            self.x_fixed_axis_full.get_line(),
            self.y_fixed_axis_full.get_line(),
        )
        #print(self.y_fixed_axis_full.numberline.get_line_direction())

        self.x_fade_extremes_config = {
            "start_fade": self.axes.get_coord_x_min() - self.x_fade_alpha_shift,
            "end_fade": self.axes.get_coord_x_max() + self.x_fade_alpha_shift,
            "fade_alpha_size": self.x_fade_alpha_size,
            "direction": RIGHT,
        }
        self.y_fade_extremes_config = {
            "start_fade": self.axes.get_coord_y_min() - self.y_fade_alpha_shift,
            "end_fade": self.axes.get_coord_y_max() + self.y_fade_alpha_shift,
            "fade_alpha_size": self.y_fade_alpha_size,
            "direction": UP
        }
        self.axes_initial_config["show_lines"] = False
        self.axes_initial_config["x_axis_config"]["avoids_normalization"] = True
        self.initial_axes = DataAxes(**self.axes_initial_config)
        #self.fade_extremes_axes(self.initial_axes)
        if self.start_point == None:
            self.start_point = self.axes.x_min
        if self.end_point == None:
            self.end_point = self.initial_axes.x_axis.v_max

        self.graph_from_data = self.get_data_graph(
            self.axes,
            self.start_point,
            self.end_point,
            shift=0,
            pre_seconds=self.pre_seconds,
            dt=self.dt_data,
            type=self.graph_type,
            graph_config=self.graph_config,
        )
        self.initial_shift_val = RIGHT * self.initial_shift * self.initial_axes.x_axis.numberline.unit_size
        self.move_initial_shift()

    def construct(self):
        self.camera_frame.save_state()
        self.add(
            self.fixed_axes,
            self.initial_axes,
            self.graph_from_data
        )
        self.shift_tracker = ValueTracker(0)
        #print("NO DEBE REPETIRSE")
        shift_group = VGroup(self.initial_axes,self.graph_from_data)
        def update_shift_group(vgroup):
            ia, gfd = vgroup
            xa = ia.x_axis
            fix_dx = abs((xa.get_numbers()[0].get_left() - xa.get_tick_marks()[-1].get_left())[0])
            dx = ia.get_x_unit_size()
            sg = VGroup(xa,gfd)
            sg_y = sg.get_y()
            try:
                sub_step = self.f_dx
            except:
                sub_step = 0
            sg.next_to(
                self.graph_origin \
                + LEFT * self.shift_tracker.get_value() * dx \
                + LEFT * (fix_dx + sub_step),
                RIGHT, buff=0)
            sg.set_y(sg_y)
            self.move_initial_shift()
            fade_config = {
                "start_fade": self.axes.get_coord_x_min() - self.x_fade_alpha_shift,
                "end_fade": self.axes.get_coord_x_max() + self.x_fade_alpha_shift,
                "fade_alpha_size": self.x_fade_alpha_size,
                "direction": RIGHT,
            }
            self.fade_extremes(gfd,type="dots",**fade_config) # FUNCIONA
            self.fade_extremes_axes(ia)
        update_shift_group(shift_group)
        if not self.start_animation:
            tick_labels_marks = self.initial_axes.x_axis.get_tick_marks()
            self.f_dx = 0
            for coord in tick_labels_marks:
                if abs(coord.get_center()[0] - self.graph_origin[0]) < 0.001:
                    #print("ENTRE")
                    #print(f"coord = {coord.get_center()[0]}")
                    #print("----")
                    self.f_dx = 0
                else:
                    self.f_dx = self.get_fix_dx()
        else:
            self.f_dx = 0
        print("f_dx: ",self.f_dx)
        shift_group.add_updater(update_shift_group)
        self.add(shift_group)
        print(f"Resolution: {self.camera.pixel_width} x {self.camera.pixel_height} at {self.camera.frame_rate} fps")
        if not self.see_initial_state:
            for anim_dict in self.animation_progress:
                key = list(anim_dict.keys())[0]
                if key == "roll":
                    values = anim_dict[key]
                    self.roll_graph(**values)
                if key == "wait":
                    print(f"WAIT {anim_dict[key]}s")
                    self.wait(anim_dict[key])
                if key == "shift":
                    values = anim_dict[key]
                    self.shift_animation(**values)
                if key == "zoom_camera":
                    values = anim_dict[key]
                    self.zoom_camera(**values)
                if key == "restore_camera":
                    values = anim_dict[key]
                    self.restore_camera(**values)
                if key == "move_camera":
                    values = anim_dict[key]
                    self.move_camera(**values)
                print("")

    def get_fix_dx(self):
        x_axis = self.initial_axes.x_axis
        numbers = x_axis.get_numbers()
        tick_marks = x_axis.get_tick_marks()
        #numbers[-1].set_color(RED)
        #tick_marks[-2].set_color(YELLOW_D)
        start_number = numbers[0].get_x()
        for i in range(len(tick_marks)):
            if abs(start_number - tick_marks[i].get_x()) < 0.01:
                j = i
                break
        dx = abs(numbers[0].get_left()[0] - tick_marks[j].get_x())
        return dx

    def move_initial_shift(self):
        VGroup(
            self.initial_axes.x_axis,
            self.graph_from_data
        ).shift(self.initial_shift_val)

    def get_new_axes(self,x_zoom,y_zoom):
        axes_config = self.axes_initial_config.copy()
        axes_config["x_axis_config"]["zoom"] = x_zoom
        axes_config["y_axis_config"]["zoom"] = y_zoom
        axes = DataAxes(
            **axes_config
        )
        #self.fade_extremes_axes(axes)
        return axes

    def get_data_graph(self,axes,start,end,shift=0,pre_seconds=0,dt=0.02,type="dots",graph_config={}):
        graph = axes.get_graph_from_data(
            self.file,
            start,
            end,
            shift,
            pre_seconds,
            dt=0.02,
            type=type,
            graph_kwargs=graph_config
        )
        self.fade_extremes(graph,type=type,**self.x_fade_extremes_config)
        return graph

    def fade_extremes(self,
                      vmob,
                      fade_alpha_size,
                      start_fade,
                      end_fade,
                      direction=RIGHT,
                      type="line",
        ):
        """
        self.fade_extremes(
            axes.get_all_y_fade_parts(),
            2, #c_fade
            -4, # coord_i
            4, # coord_f
            UP
        )
        """
        end_i   = start_fade
        start_i = (start_fade + fade_alpha_size)
        end_f   = (end_fade - fade_alpha_size)
        start_f = end_fade
        if direction[0] == 1:
            dir_coord = 0
        else:
            dir_coord = 1
        if type == "dots":
            pass
        for mob in vmob:
            also_fill = True if mob.__class__.__name__ == "DecimalTextNumber" or mob.__class__.__name__ == "Dot" else False
            coord = mob.get_coord(dir_coord)
            if end_f <= coord <= start_f:
                ds = (end_f - start_f)
                alpha = abs( (start_f - coord) / ds )
                fade = interpolate(0,1,alpha)
                if also_fill:
                    mob.set_fill(opacity=fade)
                mob.set_style(stroke_opacity=fade)
            elif start_i < coord < end_f:
                if also_fill:
                    mob.set_fill(opacity=1)
                mob.set_style(stroke_opacity=1)
            elif end_i <= coord <= start_i:
                ds = (end_i - start_i)
                alpha = abs( (start_i - coord) / ds ) 
                fade = interpolate(0,1,alpha)
                if also_fill:
                    mob.set_fill(opacity=1-fade)
                mob.set_style(stroke_opacity=1-fade)
            else:
                if also_fill:
                    mob.set_fill(opacity=0)
                mob.set_style(stroke_opacity=0)

    def fade_extremes_axes(self,axes):
        self.fade_extremes(
            axes.get_all_x_fade_parts(),
            **self.x_fade_extremes_config
        )
        self.fade_extremes(
            axes.get_all_y_fade_parts(),
            **self.y_fade_extremes_config
        )

    def shift_animation(self,target,run_time=1,rate_func=smooth):
        print("** SHIFT ANIMATION ANIMATION")
        print(f"At: {self.time}s")
        print(f"DURATION: {run_time}s")
        print(f"RATE FUNC: {rate_func.__name__}")
        if self.show_progress_data:
            print(f"***** ARGS:")
            print(f"\ttarget: {target}")
        value = target
        self.play(
            self.shift_tracker.set_value, value + self.initial_shift,
            run_time=run_time,
            rate_func=rate_func,
        )

    def roll_graph(self, target, speed=None, run_time=None):
        value = target
        init_value = self.shift_tracker.get_value()
        total_shift = value - init_value + self.initial_shift
        if speed != None and run_time == None:
            rt = abs(total_shift) / speed
        elif speed == None and run_time != None:
            rt = run_time
        elif speed == None and run_time == None:
            rt = abs(total_shift)
        print("** ROLL GRAPH ANIMATION")
        print(f"At: {self.time}s")
        print(f"DURATION: {rt}s")
        print(f"RATE FUNC: linear")
        if self.show_progress_data:
            print(f"***** ARGS:")
            print(f"\ttarget: {target}")
        self.play(
            self.shift_tracker.set_value, value + self.initial_shift,
            rate_func=linear,
            run_time=rt
        )

    def zoom_camera(self, point, height=None, scale=None, run_time=1.5, rate_func=smooth):
        camera = self.camera_frame
        x,y = point
        coord = [x,y,0]
        print("** ZOOM CAMERA ANIMATION")
        print(f"At: {self.time}s")
        print(f"DURATION: {run_time}s")
        print(f"RATE FUNC: {rate_func.__name__}")
        if height != None and scale == None:
            self.play(
                camera.set_height,height,
                camera.move_to,coord,
                rate_func=rate_func,
                run_time=run_time,
            )
        elif height == None and scale != None:
            self.play(
                camera.scale,1/scale,
                camera.move_to,coord,
                rate_func=rate_func,
                run_time=run_time,
            )
        else:
            raise "Error, scale and height can't be defined at the same time"

    def restore_camera(self,run_time=1.5, rate_func=smooth):
        print("** RESTORE CAMERA ANIMATION")
        print(f"At: {self.time}s")
        print(f"DURATION: {run_time}s")
        print(f"RATE FUNC: {rate_func.__name__}")
        self.play(Restore(self.camera_frame,run_time=run_time, rate_func=rate_func))

    def move_camera(self, point=None, shift=None, run_time=1.5, rate_func=smooth):
        camera = self.camera_frame
        print("** MOVE CAMERA ANIMATION")
        print(f"At: {self.time}s")
        print(f"DURATION: {run_time}")
        print(f"RATE FUNC: {rate_func.__name__}s")
        if self.show_progress_data:
            print(f"***** ARGS:")
        if point != None and shift == None:
            x,y = point
            if self.show_progress_data:
                print(f"\tpoint: {point}")
            self.play(
                camera.move_to,[x,y,0],
                rate_func=rate_func,
                run_time=run_time,
            )
        elif point == None and shift != None:
            x,y = shift
            if self.show_progress_data:
                print(f"\tpoint: {shift}")
            self.play(
                camera.shift,[x,y,0],
                rate_func=rate_func,
                run_time=run_time,
            )
        else:
            raise "Error, point and shift can't be defined at the same time"
        