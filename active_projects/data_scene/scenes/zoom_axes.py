from manimlib.imports import *
from active_projects.data_scene.modules.imports import *

class ZoomDataAxes(Scene):
    CONFIG = {
        "include_grid": True,
        "file": "active_projects/data_scene/data/data",
        "graph_origin": 5 * LEFT + 0 * DOWN,
        "x_axis_config": {
            "v_min": 0,
            "v_max": 8,
            "min_number": 1,
            "line_to_number_buff":2
        },
        "y_axis_config": {
            "zoom": 1,
            "v_max": 0.2,
            "v_min": -0.2,
            "number_frequency": 0.05,
            "axes_size": 6,
            "min_scale_to_show":0.95,
            "line_config": {},
            "tick_marks_config": {},
            "decimal_number_config": {
                "num_decimal_places": 2,
                "font_config": {
                    "color": TEAL
                }
            }
        },
        "x_fade_alpha_size": 2,
        "x_fade_alpha_shift": 0,
        "y_fade_alpha_size": 2,
        "y_fade_alpha_shift": 1,
        "grid_config":{}
    }
    def setup(self):
        if self.include_grid:
            self.add(ScreenGrid(**self.grid_config))
        self.axes_initial_config = {
            "graph_origin": self.graph_origin,
            "x_axis_config": self.x_axis_config,
            "y_axis_config": self.y_axis_config,
        }
        self.axes = DataAxes(**self.axes_initial_config)
        self.x_fixed_axis_full, self.y_fixed_axis_full = self.axes
        self.fixed_axes = VGroup(
            self.x_fixed_axis_full.get_line(),
            self.y_fixed_axis_full.get_line(),
        )
        #print(self.y_fixed_axis_full.numberline.get_line_direction())

        self.x_fade_extremes_config = {
            "start_fade": self.axes.get_coord_x_min() - self.x_fade_alpha_shift,
            "end_fade": self.axes.get_coord_x_max() + self.x_fade_alpha_shift,
            "fade_alpha_size": self.x_fade_alpha_size,
            "direction": RIGHT,
        }
        self.y_fade_extremes_config = {
            "start_fade": self.axes.get_coord_y_min() - self.y_fade_alpha_shift,
            "end_fade": self.axes.get_coord_y_max() + self.y_fade_alpha_shift,
            "fade_alpha_size": self.y_fade_alpha_size,
            "direction": UP
        }
        self.axes_initial_config["show_lines"] = False
        self.initial_axes = DataAxes(**self.axes_initial_config)
        #self.fade_extremes_axes(self.initial_axes)
        self.graph_from_data = self.get_data_graph(
            self.axes,
            self.axes.x_min,
            self.axes.x_max,
            #-4 + 1,
            #4 + 1,
            color=ORANGE
        )
        #self.post_axes = VGroup()
    # CONSTRUCT----------------------------------------
    def construct(self):
        screen_grid = ScreenGrid()
        self.add(
            self.fixed_axes,
            self.initial_axes,
            self.graph_from_data
        )
        self.zomm_animation(1.5,1.5)
        self.wait()
        # END CONSTRUCT----------------------------------------

    def get_new_axes(self,x_zoom,y_zoom):
        axes_config = self.axes_initial_config.copy()
        axes_config["x_axis_config"]["zoom"] = x_zoom
        axes_config["y_axis_config"]["zoom"] = y_zoom
        axes = DataAxes(
            **axes_config
        )
        #self.fade_extremes_axes(axes)
        return axes

    def get_data_graph(self,axes,start,end,shift=0,pre_seconds=0,dt=0.02,**kwargs):
        graph = axes.get_graph_from_data(
            self.file,
            start,
            end,
            shift,
            pre_seconds,
            dt=0.02,
            **kwargs
        )
        self.fade_extremes(graph,**self.x_fade_extremes_config)
        return graph

    def zomm_animation(self,x_zoom,y_zoom,*anims,**kwargs):
        self.initial_axes.generate_target()
        self.initial_axes.target.become(
            self.get_new_axes(x_zoom,y_zoom)
        )
        self.graph_from_data.generate_target()
        self.graph_from_data.target.become(
            self.get_data_graph(
                self.get_new_axes(x_zoom,y_zoom),
                self.axes.x_min,
                self.axes.x_max,
                #-4 + 1 , #shift
                #4 + 1, # pre_seconds
                color=ORANGE
            )
        )
        shift_group = VGroup(
            self.initial_axes.target.x_axis,
            self.graph_from_data.target,
        )
        #shift_group.shift(LEFT)
        self.play(
            MoveToTarget(self.initial_axes),
            MoveToTarget(self.graph_from_data),
            *anims,
            **kwargs
        )

    def fade_extremes(self,
                      vmob,
                      fade_alpha_size,
                      start_fade,
                      end_fade,
                      direction=RIGHT,
        ):
        """
        self.fade_extremes(
            axes.get_all_y_fade_parts(),
            2, #c_fade
            -4, # coord_i
            4, # coord_f
            UP
        )
        """
        end_i   = start_fade
        start_i = (start_fade + fade_alpha_size)
        end_f   = (end_fade - fade_alpha_size)
        start_f = end_fade
        if direction[0] == 1:
            dir_coord = 0
        else:
            dir_coord = 1
        for mob in vmob:
            also_fill = True if mob.__class__.__name__ == "DecimalTextNumber" else False
            coord = mob.get_coord(dir_coord)
            if end_f <= coord <= start_f:
                alpha = abs( (start_f - coord) / (end_f - start_f) )
                fade = interpolate(0,1,alpha)
                if also_fill:
                    mob.set_fill(opacity=fade)
                mob.set_style(stroke_opacity=fade)
            elif start_i < coord < end_f:
                if also_fill:
                    mob.set_fill(opacity=1)
                mob.set_style(stroke_opacity=1)
            elif end_i <= coord <= start_i:
                alpha = abs( (start_i - coord) / (end_i - start_i) ) 
                fade = interpolate(0,1,alpha)
                if also_fill:
                    mob.set_fill(opacity=1-fade)
                mob.set_style(stroke_opacity=1-fade)
            else:
                if also_fill:
                    mob.set_fill(opacity=0)
                mob.set_style(stroke_opacity=0)

    def fade_extremes_axes(self,axes):
        self.fade_extremes(
            axes.get_all_x_fade_parts(),
            **self.x_fade_extremes_config
        )
        self.fade_extremes(
            axes.get_all_y_fade_parts(),
            **self.y_fade_extremes_config
        )