from manimlib.imports import *
from active_projects.data_scene.imports import *

HEIGHT_SCREEN = 482
FPS = 12
try:
    set_custom_quality(HEIGHT_SCREEN,FPS)
except:
    pass

# DOTS

class ShiftGraphDotsFullScreen(ShiftDataGraph):
    CONFIG = {
        # DEBUB CONFIG
        "see_initial_state": False,
        "include_grid": True, # Set false to remove  the grid
        "grid_config": {},
        # DATA CONFIG
        "file": "active_projects/data_scene/data/data",
        "graph_origin": 5.5 * LEFT + 0 * DOWN,
        "initial_shift": -3,
        # GRAPH CONFIG
        "graph_type": "dots",
        "graph_config": {
            "color": RED,
            "radius": 0.02,
        },
        # X-AXIS CONFIG
        "x_axis_config": {
            # INTIAL CONFIG
            "v_min": 0,
            "v_max": 8,
            "axes_size": 12,
            "min_number": 0,
            "max_size": 30,
            # LABELS CONFIG
            "line_to_number_buff":2,
            "number_scale_val": 0.3,
            "decimal_number_config": {
                "num_decimal_places": 1,
                "unit_type": "tex", #<- Use "tex" or "font"
                "unit": r"\frac{m}{s^2}",
                "unit_custom_position": lambda mob: mob.shift([0.1,-0.05,0]).scale(1.1),
                "unit_config": {
                    "color": WHITE,
                },
                "number_config": {
                    "font": "DIN Engschrift Std",
                    "stroke_width": 0,
                }
            },
            # LINE CONFIG
            "line_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
            # TICK MARKS CONFIG
            "tick_marks_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
        },
        # Y-AXIS CONFIG
        "y_axis_config": {
            # INITIAL CONFIG
            "v_max": 0.2,
            "v_min": -0.2,
            "axes_size": 7,
            # LABELS CONFIG
            "decimal_number_config": {
                "num_decimal_places": 2,
                "unit": "y",
                "unit_type": "font", 
                # Function to correct the position of the units
                "unit_custom_position": lambda mob: mob.shift(DOWN*0.2),
                "unit_config": {
                    "color": WHITE,
                    "font": "Arial"
                },
                "number_config": {
                    "font": "DIN Engschrift Std",
                    "stroke_width": 0,
                }
            },
            # LINE CONFIG
            "line_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
            # TICK MARKS CONFIG
            "tick_marks_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
        },
        # FADE EXTREMES
        "x_fade_alpha_size": 1,
        "x_fade_alpha_shift": 0.2,
        "y_fade_alpha_size": 2,
        "y_fade_alpha_shift": 1,
        # ANIMATION PROGRESS
        "animation_progress": [
            WAIT(),
            ROLL(target=3),
            ZOOM_CAMERA(point=[1,1],scale=3),
            WAIT(2),
            MOVE_CAMERA(shift=[-3,0]),
            WAIT(),
            RESTORE_CAMERA(),
            ROLL(target=11,run_time=1),
            WAIT()
        ]
    }

class ShiftGraphDots1_3ScreenVertical(ShiftDataGraph):
    CONFIG = {
        # DEBUB CONFIG
        "see_initial_state": False,
        "include_grid": True, # Set false to remove  the grid
        "grid_config": {},
        # DATA CONFIG
        "file": "active_projects/data_scene/data/data",
        "graph_origin": 5.5 * LEFT + 2.5 * DOWN, #<- Enable the grid to see this
        "initial_shift": -3,
        # GRAPH CONFIG
        "graph_type": "dots",
        "graph_config": {
            "color": RED,
            "radius": 0.02,
        },
        # X-AXIS CONFIG
        "x_axis_config": {
            # INTIAL CONFIG
            "v_min": 0,
            "v_max": 8,
            "axes_size": 12,
            "min_number": 0,
            "max_size": 30,
            # LABELS CONFIG
            "line_to_number_buff": 0.5,
            "number_scale_val": 0.3,
            "decimal_number_config": {
                "num_decimal_places": 1,
                "unit_type": "tex",
                "unit": r"\frac{m}{s^2}",
                "unit_custom_position": lambda mob: mob.shift([0.1,-0.05,0]).scale(1.1),
                "unit_config": {
                    "color": WHITE,
                },
                "number_config": {
                    "font": "DIN Engschrift Std",
                    "stroke_width": 0,
                }
            },
            # LINE CONFIG
            "line_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
            # TICK MARKS CONFIG
            "tick_marks_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
        },
        # Y-AXIS CONFIG
        "y_axis_config": {
            # INITIAL CONFIG
            "v_max": 0.2,
            "v_min": -0.2,
            "axes_size": 2.667, #<- The height of the screen is 8 units, so, 1/3 is 2.666
            "number_frequency": 0.1,
            # LABELS CONFIG
            "decimal_number_config": {
                "num_decimal_places": 2,
                "unit": "y",
                "unit_type": "font",
                # Function to correct the position of the units
                "unit_custom_position": lambda mob: mob.shift(DOWN*0.2),
                "unit_config": {
                    "color": WHITE,
                    "font": "Arial"
                },
                "number_config": {
                    "font": "DIN Engschrift Std",
                    "stroke_width": 0,
                }
            },
            # LINE CONFIG
            "line_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
            # TICK MARKS CONFIG
            "tick_marks_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
        },
        # FADE EXTREMES
        "x_fade_alpha_size": 1,
        "x_fade_alpha_shift": 0.2,
        "y_fade_alpha_size": 2,
        "y_fade_alpha_shift": 1,
        # ANIMATION PROGRESS
        "animation_progress": [
            WAIT(),
            ROLL(target=3),
            ZOOM_CAMERA(point=[1,-3],scale=3),
            WAIT(2),
            MOVE_CAMERA(shift=[-3,0]),
            WAIT(),
            RESTORE_CAMERA(),
            ROLL(target=11,run_time=1),
            WAIT()
        ]
    }


class ShiftGraphDots1_2ScreenHorizontal(ShiftDataGraph):
    CONFIG = {
        # DEBUB CONFIG
        "see_initial_state": False,
        "include_grid": True, # Set false to remove  the grid
        "grid_config": {},
        # DATA CONFIG
        "file": "active_projects/data_scene/data/data",
        "graph_origin": ORIGIN, # (0,0)
        "initial_shift": -3,
        # GRAPH CONFIG
        "graph_type": "dots",
        "graph_config": {
            "color": RED,
            "radius": 0.02,
        },
        # X-AXIS CONFIG
        "x_axis_config": {
            # INTIAL CONFIG
            "v_min": 0,
            "v_max": 4, # <- Half of screen 
            "axes_size": 12,
            "min_number": 0,
            "max_size": 30,
            # LABELS CONFIG
            "line_to_number_buff": 0.5,
            "number_scale_val": 0.3,
            "decimal_number_config": {
                "num_decimal_places": 1,
                "unit_type": "tex",
                "unit": r"\frac{m}{s^2}",
                "unit_custom_position": lambda mob: mob.shift([0.1,-0.05,0]).scale(1.1),
                "unit_config": {
                    "color": WHITE,
                },
                "number_config": {
                    "font": "DIN Engschrift Std",
                    "stroke_width": 0,
                }
            },
            # LINE CONFIG
            "line_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
            # TICK MARKS CONFIG
            "tick_marks_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
        },
        # Y-AXIS CONFIG
        "y_axis_config": {
            # INITIAL CONFIG
            "v_max": 0.2,
            "v_min": -0.2,
            "axes_size": 7,
            "number_frequency": 0.05,
            # LABELS CONFIG
            "decimal_number_config": {
                "num_decimal_places": 2,
                "unit": "y",
                "unit_type": "font",
                # Function to correct the position of the units
                "unit_custom_position": lambda mob: mob.shift(DOWN*0.2),
                "unit_config": {
                    "color": WHITE,
                    "font": "Arial"
                },
                "number_config": {
                    "font": "DIN Engschrift Std",
                    "stroke_width": 0,
                }
            },
            # LINE CONFIG
            "line_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
            # TICK MARKS CONFIG
            "tick_marks_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
        },
        # FADE EXTREMES
        "x_fade_alpha_size": 1,
        "x_fade_alpha_shift": 0.2,
        "y_fade_alpha_size": 2,
        "y_fade_alpha_shift": 1,
        # ANIMATION PROGRESS
        "animation_progress": [
            WAIT(),
            ROLL(target=3),
            ZOOM_CAMERA(point=[1,-3],scale=3),
            WAIT(2),
            MOVE_CAMERA(shift=[-3,0]),
            WAIT(),
            RESTORE_CAMERA(),
            ROLL(target=11,run_time=1),
            WAIT()
        ]
    }

## LINE

class ShiftGraphLineFullScreen(ShiftDataGraph):
    CONFIG = {
        # DEBUB CONFIG
        "see_initial_state": False,
        "include_grid": True, # Set false to remove  the grid
        "grid_config": {},
        # DATA CONFIG
        "file": "active_projects/data_scene/data/data",
        "graph_origin": 5.5 * LEFT + 0 * DOWN,
        "initial_shift": -3,
        # GRAPH CONFIG
        "graph_type": "line", # <- Change this
        "graph_config": {
            "color": RED,
            "stroke_width": 3,
        },
        # X-AXIS CONFIG
        "x_axis_config": {
            # INTIAL CONFIG
            "v_min": 0,
            "v_max": 8,
            "axes_size": 12,
            "min_number": 0,
            "max_size": 30,
            # LABELS CONFIG
            "line_to_number_buff":2,
            "number_scale_val": 0.3,
            "decimal_number_config": {
                "num_decimal_places": 1,
                "unit": r"\frac{m}{s^2}",
                "unit_custom_position": lambda mob: mob.shift([0.1,-0.05,0]).scale(1.1),
                "unit_config": {
                    "color": WHITE,
                },
                "number_config": {
                    "font": "DIN Engschrift Std",
                    "stroke_width": 0,
                }
            },
            # LINE CONFIG
            "line_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
            # TICK MARKS CONFIG
            "tick_marks_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
        },
        # Y-AXIS CONFIG
        "y_axis_config": {
            # INITIAL CONFIG
            "v_max": 0.2,
            "v_min": -0.2,
            "axes_size": 7,
            # LABELS CONFIG
            "decimal_number_config": {
                "num_decimal_places": 2,
                "unit": "y",
                "unit_type": "font",
                # Function to correct the position of the units
                "unit_custom_position": lambda mob: mob.shift(DOWN*0.2),
                "unit_config": {
                    "color": WHITE,
                    "font": "Arial"
                },
                "number_config": {
                    "font": "DIN Engschrift Std",
                    "stroke_width": 0,
                }
            },
            # LINE CONFIG
            "line_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
            # TICK MARKS CONFIG
            "tick_marks_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
        },
        # FADE EXTREMES
        "x_fade_alpha_size": 1,
        "x_fade_alpha_shift": 0.2,
        "y_fade_alpha_size": 2,
        "y_fade_alpha_shift": 1,
        # ANIMATION PROGRESS
        "animation_progress": [
            WAIT(),
            ROLL(target=3),
            ZOOM_CAMERA(point=[1,1],scale=3),
            WAIT(2),
            MOVE_CAMERA(shift=[-3,0]),
            WAIT(),
            RESTORE_CAMERA(),
            ROLL(target=11,run_time=1),
            WAIT()
        ]
    }

class ShiftGraphLine1_3ScreenVertical(ShiftDataGraph):
    CONFIG = {
        # DEBUB CONFIG
        "see_initial_state": False,
        "include_grid": True, # Set false to remove  the grid
        "grid_config": {},
        # DATA CONFIG
        "file": "active_projects/data_scene/data/data",
        "graph_origin": 5.5 * LEFT + 2.5 * DOWN,
        "initial_shift": -3,
        # GRAPH CONFIG
        "graph_type": "line",
        "graph_config": {
            "color": TEAL,
            "stroke_width": 5,
        },
        # X-AXIS CONFIG
        "x_axis_config": {
            # INTIAL CONFIG
            "v_min": 0,
            "v_max": 8,
            "axes_size": 12,
            "min_number": 0,
            "max_size": 30,
            # LABELS CONFIG
            "line_to_number_buff": 0.5,
            "number_scale_val": 0.3,
            "decimal_number_config": {
                "num_decimal_places": 1,
                "unit_type": "tex",
                "unit": r"\frac{m}{s^2}",
                "unit_custom_position": lambda mob: mob.shift([0.1,-0.05,0]).scale(1.1),
                "unit_config": {
                    "color": WHITE,
                },
                "number_config": {
                    "font": "DIN Engschrift Std",
                    "stroke_width": 0,
                }
            },
            # LINE CONFIG
            "line_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
            # TICK MARKS CONFIG
            "tick_marks_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
        },
        # Y-AXIS CONFIG
        "y_axis_config": {
            # INITIAL CONFIG
            "v_max": 0.2,
            "v_min": -0.2,
            "axes_size": 2.667,
            "number_frequency": 0.1,
            # LABELS CONFIG
            "decimal_number_config": {
                "num_decimal_places": 2,
                "unit": "y",
                "unit_type": "font",
                # Function to correct the position of the units
                "unit_custom_position": lambda mob: mob.shift(DOWN*0.2),
                "unit_config": {
                    "color": WHITE,
                    "font": "Arial"
                },
                "number_config": {
                    "font": "DIN Engschrift Std",
                    "stroke_width": 0,
                }
            },
            # LINE CONFIG
            "line_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
            # TICK MARKS CONFIG
            "tick_marks_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
        },
        # FADE EXTREMES
        "x_fade_alpha_size": 1,
        "x_fade_alpha_shift": 0.2,
        "y_fade_alpha_size": 2,
        "y_fade_alpha_shift": 1,
        # ANIMATION PROGRESS
        "animation_progress": [
            WAIT(),
            ROLL(target=3),
            ZOOM_CAMERA(point=[1,-3],scale=3),
            WAIT(2),
            MOVE_CAMERA(shift=[-3,0]),
            WAIT(),
            RESTORE_CAMERA(),
            ROLL(target=11,run_time=1),
            WAIT()
        ]
    }


class ShiftGraphLine1_2ScreenHorizontal(ShiftDataGraph):
    CONFIG = {
        # DEBUB CONFIG
        "see_initial_state": False,
        "include_grid": True, # Set false to remove  the grid
        "grid_config": {},
        # DATA CONFIG
        "file": "active_projects/data_scene/data/data",
        "graph_origin": ORIGIN, # (0,0)
        "initial_shift": -3,
        # GRAPH CONFIG
        "graph_type": "line",
        "graph_config": {
            "color": RED,
            "stroke_width": 3,
        },
        # X-AXIS CONFIG
        "x_axis_config": {
            # INTIAL CONFIG
            "v_min": 0,
            "v_max": 4, # <- Half of screen 
            "axes_size": 6, #<- Half of screen
            "min_number": 0,
            "max_size": 30,
            # LABELS CONFIG
            "line_to_number_buff": 0.5,
            "number_scale_val": 0.3,
            "decimal_number_config": {
                "num_decimal_places": 1,
                "unit_type": "tex",
                "unit": r"\frac{m}{s^2}",
                "unit_custom_position": lambda mob: mob.shift([0.1,-0.05,0]).scale(1.1),
                "unit_config": {
                    "color": WHITE,
                },
                "number_config": {
                    "font": "DIN Engschrift Std",
                    "stroke_width": 0,
                }
            },
            # LINE CONFIG
            "line_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
            # TICK MARKS CONFIG
            "tick_marks_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
        },
        # Y-AXIS CONFIG
        "y_axis_config": {
            # INITIAL CONFIG
            "v_max": 0.2,
            "v_min": -0.2,
            "axes_size": 7,
            "number_frequency": 0.05,
            # LABELS CONFIG
            "decimal_number_config": {
                "num_decimal_places": 2,
                "unit": "y",
                "unit_type": "font",
                # Function to correct the position of the units
                "unit_custom_position": lambda mob: mob.shift(DOWN*0.2),
                "unit_config": {
                    "color": WHITE,
                    "font": "Arial"
                },
                "number_config": {
                    "font": "DIN Engschrift Std",
                    "stroke_width": 0,
                }
            },
            # LINE CONFIG
            "line_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
            # TICK MARKS CONFIG
            "tick_marks_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
        },
        # FADE EXTREMES
        "x_fade_alpha_size": 1,
        "x_fade_alpha_shift": 0.2,
        "y_fade_alpha_size": 2,
        "y_fade_alpha_shift": 1,
        # ANIMATION PROGRESS
        "animation_progress": [
            WAIT(),
            ROLL(target=3),
            ZOOM_CAMERA(point=[1,1],scale=3),
            WAIT(2),
            MOVE_CAMERA(shift=[-3,0]),
            WAIT(),
            RESTORE_CAMERA(),
            ROLL(target=11,run_time=1),
            WAIT()
        ]
    }