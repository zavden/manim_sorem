from manimlib.imports import *
from active_projects.data_scene.imports import *

INITIAL_SHIFT = None
START_POINT   = None
END_POINT     = None

D_DATA = None
HEIGHT_SCREEN = None
FPS = None
TARGET = None
START = None
RUN_TIME = None

if RUN_TIME == None:
    kwargs = {}
else:
    kwargs = {"run_time": RUN_TIME}

try:
    set_custom_quality(HEIGHT_SCREEN,FPS)
except:
    pass
# python3 -m manim active_projects\data_scene\test.py ShowData -pk
class TEST1(ShiftDataGraph):
    CONFIG = {
        # CAMERA CONFIG
        # "camera_config": {
        #     "background_color": BLACK,
        # },
        # DEBUB CONFIG
        "see_initial_state": False,
        "include_grid": True, # Set false to remove  the grid
        "grid_config": {},
        "start_animation": START, # <- DONT CHANGE *************
        "show_progress_data": False,
        # DATA CONFIG
        "file": "active_projects/data_scene/data/data",
        "graph_origin": 5.5 * LEFT + 2.5 * DOWN, #<- Enable the grid to see this
        "initial_shift": INITIAL_SHIFT, # <- DONT CHANGE *************
        "start_point": START_POINT, # <- DONT CHANGE *************
        "end_point": END_POINT + D_DATA, # <- DONT CHANGE *************
        # GRAPH CONFIG
        "graph_type": "dots",
        "graph_config": {
            "color": RED,
            "radius": 0.02,
        },
        # X-AXIS CONFIG
        "x_axis_config": {
            # INTIAL CONFIG
            "v_min": 0,
            "v_max": 8,
            "axes_size": 12,
            "min_number": START_POINT, # <- DONT CHANGE *************
            "max_size": END_POINT + D_DATA, # <- DONT CHANGE *************
            "max_number": END_POINT + D_DATA, # <- DONT CHANGE *************
            # LABELS CONFIG
            "line_to_number_buff": 0.5,
            "number_scale_val": 0.3,
            "decimal_number_config": {
                "num_decimal_places": 1,
                "unit_type": "tex",
                "unit": r"\frac{m}{s^2}",
                "unit_custom_position": lambda mob: mob.shift([0.1,-0.05,0]).scale(1.1),
                "unit_config": {
                    "color": WHITE,
                },
                "number_config": {
                    "font": "DIN Engschrift Std",
                    "stroke_width": 0,
                }
            },
            # LINE CONFIG
            "line_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
            # TICK MARKS CONFIG
            "tick_marks_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
        },
        # Y-AXIS CONFIG
        "y_axis_config": {
            # INITIAL CONFIG
            "v_max": 0.2,
            "v_min": -0.2,
            "axes_size": 2.667, #<- The height of the screen is 8 units, so, 1/3 is 2.666
            "number_frequency": 0.1,
            # LABELS CONFIG
            "decimal_number_config": {
                "num_decimal_places": 2,
                "unit": "y",
                "unit_type": "font",
                # Function to correct the position of the units
                "unit_custom_position": lambda mob: mob.shift(DOWN*0.2),
                "unit_config": {
                    "color": WHITE,
                    "font": "Arial"
                },
                "number_config": {
                    "font": "DIN Engschrift Std",
                    "stroke_width": 0,
                }
            },
            # LINE CONFIG
            "line_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
            # TICK MARKS CONFIG
            "tick_marks_config": {
                "color": WHITE,
                "stroke_width": 5,
                "stroke_opacity": 0.5
            },
        },
        # FADE EXTREMES
        "x_fade_alpha_size": 1,
        "x_fade_alpha_shift": 0.2,
        "y_fade_alpha_size": 2,
        "y_fade_alpha_shift": 1,
        # ANIMATION PROGRESS
        "animation_progress": [
            ROLL(target=TARGET,**kwargs),
            WAIT()
        ]
    }